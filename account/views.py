from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView

from account.forms import UserRegistrationForm
# Create your views here.

class SignUp(CreateView):
    form_class = UserRegistrationForm
    success_url = reverse_lazy('account:login')
    template_name = 'account/signup.html'
