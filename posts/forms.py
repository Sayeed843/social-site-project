from django import forms
from posts.models import Post
from groups.models import Group

class PostForm(forms.ModelForm):
    class Meta:
        fields = ('message', 'group')
        model = Post

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)
        user = kwargs['initial'].get('user', None)


        if user is not None:
            self.fields['group'].queryset = Group.objects.filter(memberships__user=user)
