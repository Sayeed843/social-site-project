from django import template

register = template.Library()


@register.filter(name="word_length")
def word_length(value, length='20'):
    return " ".join(value.split()[0:int(length)])


@register.filter(name='convert_int')
def convert_int(value):
    try:
        return int(value)
    except:
        return None
