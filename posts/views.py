from django.shortcuts import render
from django.contrib import messages
from django.urls import reverse_lazy
from django.urls import reverse
from django.http import Http404
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from braces.views import SelectRelatedMixin

from posts.forms import PostForm
from posts.models import Post
from groups.models import Group

from django.contrib.auth import get_user_model
User = get_user_model()

# Create your views here.
class PostList(SelectRelatedMixin, generic.ListView):
    model = Post
    select_related = ('user', 'group')
    queryset=Post.objects.all()


    def get_context_data(self, *args, **kwargs):
        # print(f"Context: {kwargs}")
        context = super(PostList, self).get_context_data(*args,**kwargs)

        if self.request.user.is_authenticated:
            context['user_groups'] = Group.objects.filter(members__in=[self.request.user])

        context['all_groups'] = Group.objects.all()


        return context

class UserPosts(generic.ListView):
    model = Post
    template_name = 'posts/user_post_list.html'

    def get_queryset(self):
        try:
            self.post_user = User.objects.prefetch_related("posts").get(username__iexact=self.kwargs.get('username'))
        except User.DoesNotExist:
            raise Http404
        else:
            return self.post_user.posts.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['post_user'] = self.post.user
        return context



class PostDetail(SelectRelatedMixin, generic.DetailView):
    model = Post
    select_related = ('user', 'group')


    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(user__username__iexact=self.kwargs.get('username'))


class CreatePost(LoginRequiredMixin, SelectRelatedMixin, generic.CreateView):
    # fields = ('message', 'group')
    model = Post
    form_class = PostForm


    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        return super().form_valid(form)

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        print("%"*10)
        context['group'] = Group.objects.filter(memberships__user=self.request.user)
        # context['group']=context['user_group'][0]
        print(f"Context:{context}")
        return context

        ## Send user argument to the form.py file
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['initial']['user'] = self.request.user
        return kwargs



class DeletePost(LoginRequiredMixin, SelectRelatedMixin, generic.DeleteView):
    model = Post
    select_related = ('user', 'group')
    # success_url = reverse_lazy('posts:all')


    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(user_id=self.request.user.id)

    def delete(self, *args, **kwargs):
        messages.success(self.request, "Post has been deleted!")
        return super().delete(*args, **kwargs)

    def get_success_url(self, *args, **kwargs):
        return reverse('posts:for_user', kwargs = {'username': self.request.user})
